
import Card from "../components/card"
import Header from "../header";
import { useEffect, useState } from "react";
import { FaTrash } from "react-icons/fa";
import "./cart.scss"
import Modal from "../components/modal";


export default function Cart ({cart,actionModal,modalToShow,closeModal,deleteCart}){
    let [its,setIt]=useState(null)

    useEffect(()=>{localStorage.setItem("cards", JSON.stringify(cart))
    if(cart==null){
        localStorage.removeItem("cards")} } )    
        
        
    if (cart!=null){
    return (
        <>

    <div className="cart">
    { cart.map(el=> 
      
       <div className="cart__card" key={Math.random()}>
<Card key={Math.random()}  name={el.name} image={el.image} 
price={el.price}
color={el.color}
article={el.article}/>
<div className="cart__fatrash"><FaTrash onClick={()=>{actionModal()

setIt(el.id)

}

} /></div>

</div>
)}

</div>

<Modal
          question={"Ви хочете видалити товар з корзини"} 
        
          active={modalToShow} 
          Click={(e)=> closeModal()}
          add={()=>{
            deleteCart(its)
            closeModal()}}
           />
</>
)
}



 
}
