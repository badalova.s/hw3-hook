
import PropTypes from 'prop-types';
import List from "../components/list"
import Header from "../header";
import { useEffect, useState } from 'react';

export default function Home ({list,actionModal,closeModal,modalToShow,addToCart,deleteFavorite,addToFavorite,cart}) {


          return (
 
          <List listproduct={list} actionModal={actionModal} cart={cart} modalToShow={modalToShow} closeModal={closeModal} addToFavorite={addToFavorite} addToCart={addToCart} deleteFavorite={deleteFavorite}/> 

         
          );
        
         
    

}
  
      