import Header from "../header";
import { useEffect, useState } from "react";
import Card from "../components/card"
import { FaTrash } from "react-icons/fa";
import "./favorite.scss"
import Modal from "../components/modal";
export default function Favorite ({favorite,actionModal,modalToShow,closeModal,deleteFavorite}){
    let [its,setIt]=useState(null)

    useEffect(()=>{localStorage.setItem("favorite", JSON.stringify(favorite))
    if(favorite==null){
        localStorage.removeItem("favorite")} } )    
        
        
    if (favorite!=null){
    return (
        <>

    <div className="favorite">
    { favorite.map(el=> 
      
       <div className="favorite__card" key={Math.random()}>
<Card key={Math.random()}  name={el.name} image={el.image} 
price={el.price}
color={el.color}
article={el.article}/>
<div className="cart__fatrash">
<FaTrash onClick={()=>{actionModal()

setIt(el.id)

}

} /></div>

</div>
)}

</div>
<Modal
          question={"Ви хочете видалити товар з обраного"} 
        
          active={modalToShow} 
          Click={(e)=> closeModal()}
          add={()=>{deleteFavorite(its)
            closeModal()}}
           />
</>)}


 
}
