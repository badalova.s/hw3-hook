
import "./header.scss"
import { FaShoppingCart } from "react-icons/fa"
import { FaStar } from "react-icons/fa"
import { Outlet, Link } from "react-router-dom";
import { FaHome } from "react-icons/fa"
export default function Header ({curent,favorites}){

     
  
          return (
             <div className="header">
              <Link to="/"><FaHome className="header__home"></FaHome></Link>
            <div className="header__button">
                <div className="header__cart">
                <Link to="/cart/#">              
    <FaShoppingCart/></Link>
          
    <span>{curent}</span>
                </div>
                <div className="header__favorites">
                <Link to="/favotire"> 
                <FaStar/></Link>
    <span>{favorites}</span>
                </div>
             </div>
             </div>
          );
        

      }