import { Component } from "react";
import "./modal.scss";
import List from "../list";
export default function Modal({Click,active,add,question}) {

       return (
        
        <div className={active ? "modal active" : "modal"} onClick={Click}>
        
        <div className={active ? "modal_content active" : "modal_content"} onClick={e=>e.stopPropagation()}>
        <div className="modal__exit" onClick={Click}>X</div>
        <div>{question}</div>
          
        
        <div className="modal_btn_conteiner">
        <button  onClick={add} className="modal_btn" >OK</button>
        </div>
        </div>
        </div>
      );
    
  }
  