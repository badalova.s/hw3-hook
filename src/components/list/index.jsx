import { Component } from "react";
import PropTypes from 'prop-types';
import Card from "../card"
import Modal from "../modal"
import "./list.scss"
import { useEffect, useState } from "react";
import ButtonAdd from "../button_add"
import ButtonFav from "../button_fav"



export default function List({cart,listproduct,actionModal,modalToShow,closeModal,addToCart,deleteFavorite,addToFavorite}) {
  let [its,setIt]=useState(null)


  if (listproduct==null) {
    return <div>loading...</div>
  } 
  else
  return (
    <>
   
            
                    <div className="list">
                        { listproduct.map((el)=> (
                          <div className="list__card" key={el.id}>
              <Card key={el.id} name={el.name} image={el.image} 
              id={el.id}
            price={el.price}
            color={el.color}
            article={el.article}/>
            
           <div className="card__button">
           <ButtonAdd Click={()=>{
            console.log(cart)
            actionModal() 
           setIt(el)
           
            
            }
            
            }/>
           <ButtonFav addDelete={(e)=>{
          if(e.target.parentNode.classList.contains('active')){
            e.target.parentNode.classList.remove('active')
            deleteFavorite(el.id)
        }else {
          
          e.target.parentNode.classList.add('active');
          addToFavorite(el)
        }
    
          
          }}
/>
                      </div>
                
                      </div>
           
                
                     ))}
                     
                      
                    
                    </div>

                    <Modal
          question={"Ви хочете додати цей товар до корзини"} 
          active={modalToShow} 
          Click={(e)=> closeModal()}
          add={(e)=>{
            addToCart(its)
            closeModal()
            }}/>          
                  
             
                   </>
             
              
                   
            
    
    
    
   
)
}
