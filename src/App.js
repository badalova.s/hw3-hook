import logo from './logo.svg';
import './App.scss';
import Home from "./pages/home";
import Cart from "./pages/cart";
import Favorite from "./pages/favotire";
import Header from './header';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useEffect, useState } from 'react';


function App() {

  
  const [listProduct,setListProduct]=useState(null)
  const [cart,setCart]=useState(null)
  const [favorite,setFavorite]=useState(null)
  const [modalToShow, setmodalToShow] = useState("");
// Фетчимо дані в ПродуктЛіст, отримуємо корзину, обране
  useEffect(() => {
    if (JSON.parse(localStorage.getItem("cards")))
     {
      setCart(JSON.parse(localStorage.getItem("cards")))
     }
     else {
      setCart([])
     }
     if (JSON.parse(localStorage.getItem("favorite")))
     {
      setFavorite(JSON.parse(localStorage.getItem("favorite")))
     }
     else {
      setFavorite([])
     }
   
    fetch("./list.json")
      .then((res) => res.json())
      .then((result) => {

        setListProduct(result);
      })

     
      
  }, []);

// Відкриття і закриття модалки
function actionModal(){
  setmodalToShow(true)
}
function closeModal(){
  setmodalToShow(false)
}



// Додавання до кошику
function addToCart(i) {
  if (cart.length!=0){
  if (cart.includes(i) )
    {
      alert("Цей товар вже є у корзині")
   
      

  }
  else {
    setCart([...cart, i])
    localStorage.setItem("cards", JSON.stringify([...cart,i]))
  
  }

  
  }
  else {
    setCart([...cart, i])
    localStorage.setItem("cards", JSON.stringify([...cart,i]))
  

}

}
// Видалення з кошику
        function deleteCart(i){
           setCart(cart.filter(el=>el.id!==i))
 } 
            
// Додавання в обране
function addToFavorite(i) {
  if (favorite.length!=0){
  if (favorite.includes(i) )
    {
      alert("Цей товар вже є у обраному")
   
      

  }
  else {
    setFavorite([...favorite, i])
    localStorage.setItem("favorite", JSON.stringify([...favorite,i]))
  
  }

  
  }
  else {
    setFavorite([...favorite, i])
    localStorage.setItem("favorite", JSON.stringify([...favorite,i]))
  

}

}
// Видалення з обранного
function deleteFavorite(i){
  setFavorite(favorite.filter(el=>el.id!==i))
} 

            

  return (  


  <BrowserRouter>
  <Header curent={cart?cart.length:0} favorites={favorite?favorite.length:0}/>
    <Routes>
        <Route index element={<Home list={listProduct} cart={cart}actionModal={actionModal} closeModal={closeModal} modalToShow={modalToShow} addToCart={addToCart} addToFavorite={addToFavorite} deleteFavorite={deleteFavorite}/>} />
        <Route path="cart" element={<Cart cart={cart} actionModal={actionModal} closeModal={closeModal} modalToShow={modalToShow} deleteCart={deleteCart} />} />
        <Route path="favotire" element={<Favorite favorite={favorite} modalToShow={modalToShow}  actionModal={actionModal} closeModal={closeModal} deleteFavorite={deleteFavorite}  />} />
      
    </Routes>
    

  </BrowserRouter>
  
  )

}


export default App;
